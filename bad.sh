if [[ `id -u` -ne 0 ]]; then
    echo
    echo
    echo "Must be run as root"
    echo
    echo
    exit
fi
